export class AuthService {
    loggedIn = false

    isAuth() {
        return this.loggedIn
    }

    login() {
        this.loggedIn = true
    }

    logout() {
        this.loggedIn = false
    }
}